package com.bawei.persona.analyse;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

@SpringBootApplication
@MapperScan(basePackages = "com.bawei.persona.analyse.mapper")
public class PersonaAnalyseApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonaAnalyseApplication.class, args);
    }

}
