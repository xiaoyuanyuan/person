package com.bawei.persona.analyse.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ProtalStats {

    private Long  category1_id;
    private Long  category2_id;
    private Long  category3_id;
    private String category1_name;
    private String category2_name;
    private String category3_name;

    private String carrier;
    private String carriername;
    private String user_gendertype;
    private String user_gender;
    private String province_id;
    private String province_name;
    private String tm_name;
    private String email;

    private String emailtype;
    private String user_age;
    private String yearbasetype;
    private String yearbasename;
    private Integer user_id;
    private Integer order_id;
    private Integer sku_num ;


   private BigDecimal order_price = BigDecimal.ZERO;

    private BigDecimal coupon_reduce_amount= BigDecimal.ZERO;

    private BigDecimal total_amount= BigDecimal.ZERO;

    private BigDecimal activity_reduce_amount= BigDecimal.ZERO;
    private String spu_name;

    private BigDecimal payment_total_amount= BigDecimal.ZERO;



}

