package com.bawei.persona.analyse.controller;


import com.bawei.persona.analyse.bean.ProtalStats;
import com.bawei.persona.analyse.service.ProtalPersonPaymentService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 * 编程：楚志高
 * 主要职责：接收客户端的请求(request)，对请求进行处理，并给客户端响应(response)
 *
 * @RestController = @Controller + @ResponseBody
 * @RequestMapping()可以加在类和方法上 加在类上，就相当于指定了访问路径的命名空间
 */
@RestController
@RequestMapping("/api/pay")
public class PayController {


    //  加入其中的服务
    @Autowired
    ProtalPersonPaymentService protalPersonPaymentService;

    //  订单分析按年龄端用户画像
    //方式2：封装对象，通过将对象转换的json格式字符串的方式 返回json数据
    @RequestMapping("/paymentbyyear")
    public Map getpaymentbyYear() {

        //调用service获取品类交易额排行
        List<ProtalStats> totalamountByYea = protalPersonPaymentService.getTotalamountByYea1();

        Map resMap = new HashMap();
        resMap.put("status", 0);
        List dataList = new ArrayList();
        for (ProtalStats productStats : totalamountByYea) {
            Map dataMap = new HashMap();
            dataMap.put("name", productStats.getYearbasename());
            dataMap.put("value", productStats.getPayment_total_amount());
            dataList.add(dataMap);
        }

        resMap.put("data", dataList);
        return resMap;

    }



    //  订单分析按性别端用户画像
    //方式2：封装对象，通过将对象转换的json格式字符串的方式 返回json数据
    @RequestMapping("/paymentbysex")
    public Map getpaymentbySex() {

        //调用service获取品类交易额排行
        List<ProtalStats> totalamountByYea = protalPersonPaymentService.getTotalamountBySex1();
        Map resMap = new HashMap();
        resMap.put("status", 0);
        List dataList = new ArrayList();
        for (ProtalStats productStats : totalamountByYea) {
            Map dataMap = new HashMap();
            dataMap.put("name", productStats.getUser_gender());
            dataMap.put("value", productStats.getPayment_total_amount());
            dataList.add(dataMap);
        }

        resMap.put("data", dataList);
        return resMap;

    }



    //  订单分析按性别端用户画像
    //方式2：封装对象，通过将对象转换的json格式字符串的方式 返回json数据
    @RequestMapping("/paymentbycarry")
    public Map getpaymentbyCarry() {

        //调用service获取品类交易额排行
        List<ProtalStats> totalamountByYea = protalPersonPaymentService.getTotalamountByCarry1();
        Map resMap = new HashMap();
        resMap.put("status", 0);
        List dataList = new ArrayList();
        for (ProtalStats productStats : totalamountByYea) {
            Map dataMap = new HashMap();
            dataMap.put("name", productStats.getCarriername());
            dataMap.put("value", productStats.getPayment_total_amount());
            dataList.add(dataMap);
        }

        resMap.put("data", dataList);
        return resMap;


    }

    //  订单分析按性别端用户画像
    //方式2：封装对象，通过将对象转换的json格式字符串的方式 返回json数据
    @RequestMapping("/paymentbyemail")
    public Map getpaymentbyEmal() {
        List<ProtalStats> totalamountByYea = protalPersonPaymentService.getTotalamountByEmal1();
        Map resMap = new HashMap();
        resMap.put("status", 0);
        List dataList = new ArrayList();
        for (ProtalStats productStats : totalamountByYea) {
            Map dataMap = new HashMap();
            dataMap.put("name", productStats.getEmail());
            dataMap.put("value", productStats.getPayment_total_amount());
            dataList.add(dataMap);
        }

        resMap.put("data", dataList);
        return resMap;


    }


    @RequestMapping("/paymentbyprovince")
    public String getpaymentbyProvince() {

        //从service中获取地区统计数据
        List<ProtalStats> provinceStatsList = protalPersonPaymentService.getTotalamountByArea1();
        StringBuilder jsonBuilder = new StringBuilder("{\"status\": 0,\"data\": {\"mapData\": [");

        for (int i = 0; i < provinceStatsList.size(); i++) {
            ProtalStats   provinceStats = provinceStatsList.get(i);

            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{\"name\": \"" + provinceStats.getProvince_name() + "\",\"value\": " + provinceStats.getPayment_total_amount() + "}");
        }
        jsonBuilder.append("]}}");
        return jsonBuilder.toString();

    }

    //按分类 1 23 进行排名 分裂
    @RequestMapping("/paymentbycate1")
    public Map getpaymentbycate1() {

        //调用service根据品牌获取交易额排名
        List<ProtalStats> totalamountByCate1 = protalPersonPaymentService.getTotalamountByCate11();
        //定义两个集合，分别存放品牌的名称以及品牌的交易额
        List<String> trademarkNameList = new ArrayList<>();
        List<BigDecimal> amountList = new ArrayList<>();
        //对获取到的品牌交易额进行遍历
        for (ProtalStats productStats : totalamountByCate1) {
            trademarkNameList.add(productStats.getCategory1_name());
            amountList.add(productStats.getPayment_total_amount());
        }
        Map resMap = new HashMap();
        resMap.put("status", 0);
        Map dataMap = new HashMap();
        dataMap.put("categories", trademarkNameList);
        List seriesList = new ArrayList();
        Map seriesDataMap = new HashMap();
        seriesDataMap.put("data", amountList);
        seriesList.add(seriesDataMap);
        dataMap.put("series", seriesList);
        resMap.put("data", dataMap);
        return resMap;

    }

    @RequestMapping("/paymentbycate2")
    public Map getpaymentbycate2() {

        //调用service根据品牌获取交易额排名
        List<ProtalStats> totalamountByCate1 = protalPersonPaymentService.getTotalamountByCate21();
        //定义两个集合，分别存放品牌的名称以及品牌的交易额
        List<String> trademarkNameList = new ArrayList<>();
        List<BigDecimal> amountList = new ArrayList<>();
        //对获取到的品牌交易额进行遍历
        for (ProtalStats productStats : totalamountByCate1) {
            trademarkNameList.add(productStats.getCategory2_name());
            amountList.add(productStats.getPayment_total_amount());
        }
        Map resMap = new HashMap();
        resMap.put("status", 0);
        Map dataMap = new HashMap();
        dataMap.put("categories", trademarkNameList);
        List seriesList = new ArrayList();
        Map seriesDataMap = new HashMap();
        seriesDataMap.put("data", amountList);
        seriesList.add(seriesDataMap);
        dataMap.put("series", seriesList);
        resMap.put("data", dataMap);
        return resMap;

    }

    //按第3大分类进行统计
    @RequestMapping("/paymentbycate3")
    public Map getpaymentbycate3() {

        //调用service根据品牌获取交易额排名
        List<ProtalStats> totalamountByCate1 = protalPersonPaymentService.getTotalamountByCate31();
        //定义两个集合，分别存放品牌的名称以及品牌的交易额
        List<String> trademarkNameList = new ArrayList<>();
        List<BigDecimal> amountList = new ArrayList<>();
        //对获取到的品牌交易额进行遍历
        for (ProtalStats productStats : totalamountByCate1) {
            trademarkNameList.add(productStats.getCategory3_name());
            amountList.add(productStats.getPayment_total_amount());
        }
        Map resMap = new HashMap();
        resMap.put("status", 0);
        Map dataMap = new HashMap();
        dataMap.put("categories", trademarkNameList);
        List seriesList = new ArrayList();
        Map seriesDataMap = new HashMap();
        seriesDataMap.put("data", amountList);
        seriesList.add(seriesDataMap);
        dataMap.put("series", seriesList);
        resMap.put("data", dataMap);
        return resMap;


    }

    //按品牌进行排名分类

    @RequestMapping("/paymentbytmname")
    public Map getpaymentbytmname() {
        //调用service根据品牌获取交易额排名
        List<ProtalStats> totalamountByCate1 = protalPersonPaymentService.getTotalamountByTmname1();
        //定义两个集合，分别存放品牌的名称以及品牌的交易额
        List<String> trademarkNameList = new ArrayList<>();
        List<BigDecimal> amountList = new ArrayList<>();
        //对获取到的品牌交易额进行遍历
        for (ProtalStats productStats : totalamountByCate1) {
            trademarkNameList.add(productStats.getTm_name());
            amountList.add(productStats.getPayment_total_amount());
        }
        Map resMap = new HashMap();
        resMap.put("status", 0);
        Map dataMap = new HashMap();
        dataMap.put("categories", trademarkNameList);
        List seriesList = new ArrayList();
        Map seriesDataMap = new HashMap();
        seriesDataMap.put("data", amountList);
        seriesList.add(seriesDataMap);
        dataMap.put("series", seriesList);
        resMap.put("data", dataMap);
        return resMap;

    }

    //主力客户群体按性别与年龄段进行统计分析
    // 3个字段
    @RequestMapping("/paymentbysexyear")
    public String getpaymentbysexyear() {
        //调用service层方法，获取按spu统计数据
        List<ProtalStats> totalamountByYearSex = protalPersonPaymentService.getTotalamountByYearSex1();

        //初始化表头信息
        StringBuilder jsonBuilder = new StringBuilder("{" +
                "\"status\": 0," +
                "\"data\": {" +
                "\"columns\": [{" +
                "\"name\": \"性别\"," +
                "\"id\": \"user_gender\"" +
                "}," +
                "{" +
                "\"name\": \"年龄段\"," +
                "\"id\": \"yearbasename\"" +
                "}," +
                "{" +
                "\"name\": \"总金额\"," +
                "\"id\": \"total_amount\"" +
                "}" +
                "]," +
                "\"rows\": [");
        //对查询出来的数据进行遍历，将每一条遍历的结果封装为json的一行数据
        for (int i = 0; i < totalamountByYearSex.size(); i++) {
            ProtalStats productStats = totalamountByYearSex.get(i);

            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{" +
                    "\"user_gender\": \"" + productStats.getUser_gender() + "\"," +
                    "\"yearbasename\": \"" + productStats.getYearbasename() + "\"," +
                    "\"total_amount\":" + productStats.getPayment_total_amount() + "}"
            );
        }

        jsonBuilder.append("]}}");
        return jsonBuilder.toString();


    }

    //商品的偏好 对年龄的偏好
    // tm_name,yearbasename, sum(total_amount) AS total_amount
    @RequestMapping("/goodpaybyear")
    public String getGoodpaybyyear() {

        //调用service层方法，获取按spu统计数据
        List<ProtalStats> totalamountByYearSex = protalPersonPaymentService.getTotalamountByTmnameYear1();

        //初始化表头信息
        StringBuilder jsonBuilder = new StringBuilder("{" +
                "\"status\": 0," +
                "\"data\": {" +
                "\"columns\": [{" +
                "\"name\": \"品牌\"," +
                "\"id\": \"tm_name\"" +
                "}," +
                "{" +
                "\"name\": \"年龄段\"," +
                "\"id\": \"yearbasename\"" +
                "}," +
                "{" +
                "\"name\": \"总金额\"," +
                "\"id\": \"total_amount\"" +
                "}" +
                "]," +
                "\"rows\": [");
        //对查询出来的数据进行遍历，将每一条遍历的结果封装为json的一行数据
        for (int i = 0; i < totalamountByYearSex.size(); i++) {
            ProtalStats productStats = totalamountByYearSex.get(i);

            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{" +
                    "\"tm_name\": \"" + productStats.getTm_name() + "\"," +
                    "\"yearbasename\": \"" + productStats.getYearbasename() + "\"," +
                    "\"total_amount\":" + productStats.getPayment_total_amount() + "}"
            );
        }

        jsonBuilder.append("]}}");
        return jsonBuilder.toString();

    }


    //商品的偏好 对年龄的偏好
    //  tm_name, user_gender, sum(total_amount) AS total_amount
    @RequestMapping("/goodpaybysex")
    public String getGoodpaybySex() {

        //调用service层方法，获取按spu统计数据
        List<ProtalStats> totalamountByYearSex = protalPersonPaymentService.getTotalamountByTmnameSex1();

        //初始化表头信息
        StringBuilder jsonBuilder = new StringBuilder("{" +
                "\"status\": 0," +
                "\"data\": {" +
                "\"columns\": [{" +
                "\"name\": \"品牌\"," +
                "\"id\": \"tm_name\"" +
                "}," +
                "{" +
                "\"name\": \"性别\"," +
                "\"id\": \"user_gender\"" +
                "}," +
                "{" +
                "\"name\": \"总金额\"," +
                "\"id\": \"total_amount\"" +
                "}" +
                "]," +
                "\"rows\": [");
        //对查询出来的数据进行遍历，将每一条遍历的结果封装为json的一行数据
        for (int i = 0; i < totalamountByYearSex.size(); i++) {
            ProtalStats productStats = totalamountByYearSex.get(i);

            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{" +
                    "\"tm_name\": \"" + productStats.getTm_name() + "\"," +
                    "\"user_gender\": \"" + productStats.getUser_gender() + "\"," +
                    "\"total_amount\":" + productStats.getPayment_total_amount() + "}"
            );
        }

        jsonBuilder.append("]}}");
        return jsonBuilder.toString();


    }

    //商品的偏好 对地区的偏好
    //  SELECT tm_name, province_name, sum(total_amount) AS total_amount
    @RequestMapping("/goodpaybyarea")
    public String getGoodpaybyarea() {

        //调用service层方法，获取按spu统计数据
        List<ProtalStats> totalamountByYearSex = protalPersonPaymentService.getTotalamountByTmnameArea1();

        //初始化表头信息
        StringBuilder jsonBuilder = new StringBuilder("{" +
                "\"status\": 0," +
                "\"data\": {" +
                "\"columns\": [{" +
                "\"name\": \"品牌\"," +
                "\"id\": \"tm_name\"" +
                "}," +
                "{" +
                "\"name\": \"省份\"," +
                "\"id\": \"province_name\"" +
                "}," +
                "{" +
                "\"name\": \"总金额\"," +
                "\"id\": \"total_amount\"" +
                "}" +
                "]," +
                "\"rows\": [");
        //对查询出来的数据进行遍历，将每一条遍历的结果封装为json的一行数据
        for (int i = 0; i < totalamountByYearSex.size(); i++) {
            ProtalStats productStats = totalamountByYearSex.get(i);

            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{" +
                    "\"tm_name\": \"" + productStats.getTm_name() + "\"," +
                    "\"province_name\": \"" + productStats.getProvince_name() + "\"," +
                    "\"total_amount\":" + productStats.getPayment_total_amount() + "}"
            );
        }

        jsonBuilder.append("]}}");
        return jsonBuilder.toString();

    }

    //商品对年龄段的偏好


    //商品的偏好 对年龄的偏好
    // tm_name,yearbasename, sum(total_amount) AS total_amount
    @RequestMapping("/goodpaymentbyear")
    public String goodpaymentbyear() {

        //调用service层方法，获取按spu统计数据
        List<ProtalStats> totalamountByYearSex = protalPersonPaymentService.getTotalamountByTmnameYear1();

        //初始化表头信息
        StringBuilder jsonBuilder = new StringBuilder("{" +
                "\"status\": 0," +
                "\"data\": {" +
                "\"columns\": [{" +
                "\"name\": \"品牌\"," +
                "\"id\": \"tm_name\"" +
                "}," +
                "{" +
                "\"name\": \"年龄段\"," +
                "\"id\": \"yearbasename\"" +
                "}," +
                "{" +
                "\"name\": \"总金额\"," +
                "\"id\": \"total_amount\"" +
                "}" +
                "]," +
                "\"rows\": [");
        //对查询出来的数据进行遍历，将每一条遍历的结果封装为json的一行数据
        for (int i = 0; i < totalamountByYearSex.size(); i++) {
            ProtalStats productStats = totalamountByYearSex.get(i);

            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{" +
                    "\"tm_name\": \"" + productStats.getTm_name() + "\"," +
                    "\"yearbasename\": \"" + productStats.getYearbasename() + "\"," +
                    "\"total_amount\":" + productStats.getPayment_total_amount() + "}"
            );
        }

        jsonBuilder.append("]}}");
        return jsonBuilder.toString();


    }

    ////////////////////////


    private Integer now() {
        String yyyyMMdd = DateFormatUtils.format(new Date(), "yyyyMMdd");
        return Integer.valueOf(yyyyMMdd);
    }

}
