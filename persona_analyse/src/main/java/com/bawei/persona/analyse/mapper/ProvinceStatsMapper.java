package com.bawei.persona.analyse.mapper;


import com.bawei.persona.analyse.bean.ProvinceStats;
import org.apache.ibatis.annotations.Select;

import java.util.List;



/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

public interface ProvinceStatsMapper {
    @Select("select province_name,sum(order_amount) order_amount from province_stats_2021 " +
        "where toYYYYMMDD(stt)=#{date} group by province_id,province_name")
    List<ProvinceStats> selectProvinceStats(int date);
}
