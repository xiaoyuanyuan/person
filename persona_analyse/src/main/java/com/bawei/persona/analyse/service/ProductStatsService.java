package com.bawei.persona.analyse.service;



import com.bawei.persona.analyse.bean.ProductStats;

import java.math.BigDecimal;
import java.util.List;



/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

public interface ProductStatsService {
    //获取某一天交易总额
    BigDecimal getGMV(int date);

    //获取某一天不同品牌的交易额
    List<ProductStats> getProductStatsByTrademark(int date, int limit);

    //获取某一天不同品类的交易额
    List<ProductStats> getProductStatsByCategory3(int date, int limit);

    //获取某一天不同SPU的交易额
    List<ProductStats> getProductStatsBySPU(int date, int limit);
}
