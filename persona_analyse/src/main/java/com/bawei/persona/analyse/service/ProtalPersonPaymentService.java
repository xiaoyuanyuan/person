package com.bawei.persona.analyse.service;



import com.bawei.persona.analyse.bean.ProtalStats;

import java.util.List;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

/**
 *
 * 基于人的因数对用户画像分析
 * //基于人的用户画像的统计型数据分析
 * 这里的参数类型，如果是订单表与支付表的区别，对应不同的sql
 */
public interface ProtalPersonPaymentService {

    //获取按年龄段进行销售额度的比重分析
    //下面的统计是单一的维度的进行统计
    List<ProtalStats> getTotalamountByYea1();
    List<ProtalStats> getTotalamountBySex1();
    List<ProtalStats> getTotalamountByArea1();
    List<ProtalStats> getTotalamountByCarry1();
    List<ProtalStats> getTotalamountByEmal1();
    List<ProtalStats> getTotalamountByCate11();
    List<ProtalStats> getTotalamountByCate21();
    List<ProtalStats> getTotalamountByCate31();
    List<ProtalStats> getTotalamountByTmname1();
    //下面的维度统计是按2个维度进行统计
    List<ProtalStats> getTotalamountByYearSex1();
    List<ProtalStats> getTotalamountByYearArea1();


    List<ProtalStats> getTotalamountByCarryYear1();
    List<ProtalStats> getTotalamountByCarrySex1();
    List<ProtalStats> getTotalamountByCarryArea1();
    //品牌对人性的带货
    List<ProtalStats> getTotalamountByTmnameYear1();
    List<ProtalStats> getTotalamountByTmnameSex1();
    List<ProtalStats> getTotalamountByTmnameArea1();
    //人性3个维度的组合
    List<ProtalStats> getTotalamountByYearSexArea1();
    //首单减免与折扣减免的用户画像只与人性有关,与运营商，与品牌可能绑定
    List<ProtalStats> getCouponByYea1();
    List<ProtalStats> getCouponBySex1();
    List<ProtalStats> getCouponByArea1();
    List<ProtalStats> getCouponByCarry1();
    List<ProtalStats> getCouponByTmname1();
    List<ProtalStats> getCouponByYearSex1();
    List<ProtalStats> getCouponByYearArea1();
    List<ProtalStats> getCouponBySexArea1();
    List<ProtalStats> getCouponByYearSexArea1();
    //系统按电商的分类级别进行统计用户画像
    List<ProtalStats> getCouponByByCate11();
    List<ProtalStats> getCouponByByCate21();
    List<ProtalStats> getCouponByByCate31();
    //折扣看品牌与人性结合进行减免可能减免
    List<ProtalStats> getCouponByTmnameYea1();
    List<ProtalStats> getCouponByTmnamSex1();
    List<ProtalStats> getCouponByTmnameArea1();

    // 折扣针对人性，电商配合平台分类，品牌针对人性进行折扣活动
    List<ProtalStats> getActivtyByYea1();
    List<ProtalStats> getActivtyBySex1();
    List<ProtalStats> getActivtyByArea1();
    List<ProtalStats> getActivtyByCarry1();
    List<ProtalStats> getActivtyByTmname1();
    List<ProtalStats> getActivtyByYearSex1();
    List<ProtalStats> getActivtyByYearArea1();
    List<ProtalStats> getActivtyBySexArea1();
    List<ProtalStats> getActivtyByYearSexArea1();
    //系统按电商的分类级别进行统计用户画像
    List<ProtalStats> getActivtyByByCate11();
    List<ProtalStats> getActivtyByByCate21();
    List<ProtalStats> getActivtyByByCate31();
    //折扣看品牌与人性结合进行减免可能减免
    List<ProtalStats> getActivtyByTmnameYea1();
    List<ProtalStats> getActivtyByTmnamSex1();
    List<ProtalStats> getActivtyByTmnameArea1();
    List<ProtalStats> getGoodTop1();



}
