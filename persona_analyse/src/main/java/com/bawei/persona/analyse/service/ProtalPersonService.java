package com.bawei.persona.analyse.service;



import com.bawei.persona.analyse.bean.ProtalStats;

import java.util.List;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

/**
 *
 * 基于人的因数对用户画像分析
 * //基于人的用户画像的统计型数据分析
 * 这里的参数类型，如果是订单表与支付表的区别，对应不同的sql
 */
public interface ProtalPersonService {

    //获取按年龄段进行销售额度的比重分析
    //下面的统计是单一的维度的进行统计
    List<ProtalStats> getTotalamountByYea();
    List<ProtalStats> getTotalamountBySex();
    List<ProtalStats> getTotalamountByArea();
    List<ProtalStats> getTotalamountByCarry();
    List<ProtalStats> getTotalamountByEmal();
    List<ProtalStats> getTotalamountByCate1();
    List<ProtalStats> getTotalamountByCate2();
    List<ProtalStats> getTotalamountByCate3();
    List<ProtalStats> getTotalamountByTmname();
    //下面的维度统计是按2个维度进行统计
    List<ProtalStats> getTotalamountByYearSex();
    List<ProtalStats> getTotalamountByYearArea();


    List<ProtalStats> getTotalamountByCarryYear();
    List<ProtalStats> getTotalamountByCarrySex();
    List<ProtalStats> getTotalamountByCarryArea();
    //三个维度，性别 年龄 商品品牌
    //品牌对人性的带货
    List<ProtalStats> getTotalamountByTmnameYear();
    List<ProtalStats> getTotalamountByTmnameSex();
    List<ProtalStats> getTotalamountByTmnameArea();
    //人性3个维度的组合
    List<ProtalStats> getTotalamountByYearSexArea();
    //首单减免与折扣减免的用户画像只与人性有关,与运营商，与品牌可能绑定
    List<ProtalStats> getCouponByYea();
    List<ProtalStats> getCouponBySex();
    List<ProtalStats> getCouponByArea();
    List<ProtalStats> getCouponByCarry();
    List<ProtalStats> getCouponByTmname();
    List<ProtalStats> getCouponByYearSex();
    List<ProtalStats> getCouponByYearArea();
    List<ProtalStats> getCouponBySexArea();
    List<ProtalStats> getCouponByYearSexArea();
    //系统按电商的分类级别进行统计用户画像
    List<ProtalStats> getCouponByByCate1();
    List<ProtalStats> getCouponByByCate2();
    List<ProtalStats> getCouponByByCate3();
    //折扣看品牌与人性结合进行减免可能减免
    List<ProtalStats> getCouponByTmnameYea();
    List<ProtalStats> getCouponByTmnamSex();
    List<ProtalStats> getCouponByTmnameArea();


    // 折扣针对人性，电商配合平台分类，品牌针对人性进行折扣活动
    List<ProtalStats> getActivtyByYea();
    List<ProtalStats> getActivtyBySex();
    List<ProtalStats> getActivtyByArea();
    List<ProtalStats> getActivtyByCarry();
    List<ProtalStats> getActivtyByTmname();
    List<ProtalStats> getActivtyByYearSex();
    List<ProtalStats> getActivtyByYearArea();
    List<ProtalStats> getActivtyBySexArea();
    List<ProtalStats> getActivtyByYearSexArea();
    //系统按电商的分类级别进行统计用户画像
    List<ProtalStats> getActivtyByByCate1();
    List<ProtalStats> getActivtyByByCate2();
    List<ProtalStats> getActivtyByByCate3();
    //折扣看品牌与人性结合进行减免可能减免
    List<ProtalStats> getActivtyByTmnameYea();
    List<ProtalStats> getActivtyByTmnamSex();
    List<ProtalStats> getActivtyByTmnameArea();


}
