package com.bawei.persona.analyse.service.impl;


import com.bawei.persona.analyse.bean.ProtalStats;
import com.bawei.persona.analyse.mapper.ProtalPersonPaymentMapper;
import com.bawei.persona.analyse.service.ProtalPersonPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */


@Service
public class ProtalPersonPaymentServiceImpl implements ProtalPersonPaymentService {
    //注入mapper
    @Autowired
    ProtalPersonPaymentMapper protalPersonPaymentMapper;


    @Override
    public List<ProtalStats> getTotalamountByYea1() {
        return protalPersonPaymentMapper.getTotalamountByYea1();
    }

    @Override
    public List<ProtalStats> getTotalamountBySex1() {
        return protalPersonPaymentMapper.getTotalamountBySex1();
    }

    @Override
    public List<ProtalStats> getTotalamountByArea1() {
        return protalPersonPaymentMapper.getTotalamountByArea1();
    }

    @Override
    public List<ProtalStats> getTotalamountByCarry1() {
        return protalPersonPaymentMapper.getTotalamountByCarry1();
    }

    @Override
    public List<ProtalStats> getTotalamountByEmal1() {
        return protalPersonPaymentMapper.getTotalamountByEmal1();
    }

    @Override
    public List<ProtalStats> getTotalamountByCate11() {
        return protalPersonPaymentMapper.getTotalamountByCate11();
    }

    @Override
    public List<ProtalStats> getTotalamountByCate21() {
        return this.protalPersonPaymentMapper.getTotalamountByCate21();
    }

    @Override
    public List<ProtalStats> getTotalamountByCate31() {
        return this.protalPersonPaymentMapper.getTotalamountByCate31();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmname1() {
        return this.protalPersonPaymentMapper.getTotalamountByTmname1();
    }

    @Override
    public List<ProtalStats> getTotalamountByYearSex1() {
        return this.protalPersonPaymentMapper.getTotalamountByYearSex1();
    }

    @Override
    public List<ProtalStats> getTotalamountByYearArea1() {
        return this.protalPersonPaymentMapper.getTotalamountByYearArea1();
    }


    @Override
    public List<ProtalStats> getTotalamountByCarryYear1() {
        return this.protalPersonPaymentMapper.getTotalamountByCarryYear1();
    }

    @Override
    public List<ProtalStats> getTotalamountByCarrySex1() {
        return this.protalPersonPaymentMapper.getTotalamountByCarrySex1();
    }

    @Override
    public List<ProtalStats> getTotalamountByCarryArea1() {
        return this.protalPersonPaymentMapper.getTotalamountByCarryArea1();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmnameYear1() {
        return this.protalPersonPaymentMapper.getTotalamountByTmnameYear();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmnameSex1() {
        return this.protalPersonPaymentMapper.getTotalamountByTmnameSex1();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmnameArea1() {
        return this.protalPersonPaymentMapper.getTotalamountByTmnameArea1();
    }

    @Override
    public List<ProtalStats> getTotalamountByYearSexArea1() {
        return this.protalPersonPaymentMapper.getTotalamountByYearSexArea1();
    }

    @Override
    public List<ProtalStats> getCouponByYea1() {
        return this.protalPersonPaymentMapper.getCouponByYea1();
    }

    @Override
    public List<ProtalStats> getCouponBySex1() {
        return this.protalPersonPaymentMapper.getCouponBySex1();
    }

    @Override
    public List<ProtalStats> getCouponByArea1() {
        return this.protalPersonPaymentMapper.getCouponByArea1();
    }

    @Override
    public List<ProtalStats> getCouponByCarry1() {
        return protalPersonPaymentMapper.getCouponByCarry1();
    }

    @Override
    public List<ProtalStats> getCouponByTmname1() {
        return protalPersonPaymentMapper.getCouponByTmname1();
    }

    @Override
    public List<ProtalStats> getCouponByYearSex1() {
        return protalPersonPaymentMapper.getCouponByYearSex1();
    }

    @Override
    public List<ProtalStats> getCouponByYearArea1() {
        return protalPersonPaymentMapper.getCouponByYearArea1();
    }

    @Override
    public List<ProtalStats> getCouponBySexArea1() {
        return protalPersonPaymentMapper.getCouponBySexArea();
    }

    @Override
    public List<ProtalStats> getCouponByYearSexArea1() {
        return protalPersonPaymentMapper.getCouponByYearSexArea1();
    }

    @Override
    public List<ProtalStats> getCouponByByCate11() {
        return protalPersonPaymentMapper.getCouponByByCate11();
    }

    @Override
    public List<ProtalStats> getCouponByByCate21() {
        return protalPersonPaymentMapper.getCouponByByCate21();
    }

    @Override
    public List<ProtalStats> getCouponByByCate31() {
        return protalPersonPaymentMapper.getCouponByByCate3();
    }

    @Override
    public List<ProtalStats> getCouponByTmnameYea1() {
        return protalPersonPaymentMapper.getCouponByTmnameYea1();
    }

    @Override
    public List<ProtalStats> getCouponByTmnamSex1() {
        return protalPersonPaymentMapper.getCouponByTmnamSex1();
    }

    @Override
    public List<ProtalStats> getCouponByTmnameArea1() {
        return protalPersonPaymentMapper.getCouponByTmnameArea1();
    }

    @Override
    public List<ProtalStats> getActivtyByYea1() {
        return protalPersonPaymentMapper.getActivtyByYea1();
    }

    @Override
    public List<ProtalStats> getActivtyBySex1() {
        return protalPersonPaymentMapper.getActivtyBySex1();
    }

    @Override
    public List<ProtalStats> getActivtyByArea1() {
        return protalPersonPaymentMapper.getActivtyByArea1();
    }

    @Override
    public List<ProtalStats> getActivtyByCarry1() {
        return protalPersonPaymentMapper.getActivtyByCarry1();
    }

    @Override
    public List<ProtalStats> getActivtyByTmname1() {
        return protalPersonPaymentMapper.getActivtyByTmname1();
    }

    @Override
    public List<ProtalStats> getActivtyByYearSex1() {
        return protalPersonPaymentMapper.getActivtyByYearSex1();
    }

    @Override
    public List<ProtalStats> getActivtyByYearArea1() {
        return protalPersonPaymentMapper.getActivtyByYearArea1();
    }

    @Override
    public List<ProtalStats> getActivtyBySexArea1() {
        return protalPersonPaymentMapper.getActivtyBySexArea1();
    }

    @Override
    public List<ProtalStats> getActivtyByYearSexArea1() {
        return protalPersonPaymentMapper.getActivtyByYearSexArea1();
    }

    @Override
    public List<ProtalStats> getActivtyByByCate11() {
        return protalPersonPaymentMapper.getActivtyByByCate11();
    }

    @Override
    public List<ProtalStats> getActivtyByByCate21() {
        return protalPersonPaymentMapper.getActivtyByByCate21();
    }

    @Override
    public List<ProtalStats> getActivtyByByCate31() {
        return protalPersonPaymentMapper.getActivtyByByCate31();
    }

    @Override
    public List<ProtalStats> getActivtyByTmnameYea1() {
        return protalPersonPaymentMapper.getActivtyByTmnameYea1();
    }

    @Override
    public List<ProtalStats> getActivtyByTmnamSex1() {
        return protalPersonPaymentMapper.getActivtyByTmnamSex1();
    }

    @Override
    public List<ProtalStats> getActivtyByTmnameArea1() {
        return protalPersonPaymentMapper.getActivtyByTmnameArea1();
    }

    @Override
    public List<ProtalStats> getGoodTop1() {
        return protalPersonPaymentMapper.getGoodTop1();
    }
}
