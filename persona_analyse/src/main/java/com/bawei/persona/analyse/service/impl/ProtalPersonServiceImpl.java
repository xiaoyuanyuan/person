package com.bawei.persona.analyse.service.impl;


import com.bawei.persona.analyse.bean.ProtalStats;
import com.bawei.persona.analyse.mapper.ProtalPersonMapper;
import com.bawei.persona.analyse.service.ProtalPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */


@Service
public class ProtalPersonServiceImpl implements ProtalPersonService {
    //注入mapper
    @Autowired
    ProtalPersonMapper protalPersonMapper;


    @Override
    public List<ProtalStats> getTotalamountByYea() {
        return protalPersonMapper.getTotalamountByYea();
    }

    @Override
    public List<ProtalStats> getTotalamountBySex() {
        return protalPersonMapper.getTotalamountBySex();
    }

    @Override
    public List<ProtalStats> getTotalamountByArea() {
        return protalPersonMapper.getTotalamountByArea();
    }

    @Override
    public List<ProtalStats> getTotalamountByCarry() {
        return protalPersonMapper.getTotalamountByCarry();
    }

    @Override
    public List<ProtalStats> getTotalamountByEmal() {
        return protalPersonMapper.getTotalamountByEmal();
    }

    @Override
    public List<ProtalStats> getTotalamountByCate1() {
        return protalPersonMapper.getTotalamountByCate1();
    }

    @Override
    public List<ProtalStats> getTotalamountByCate2() {
        return this.protalPersonMapper.getTotalamountByCate2();
    }

    @Override
    public List<ProtalStats> getTotalamountByCate3() {
        return this.protalPersonMapper.getTotalamountByCate3();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmname() {
        return this.protalPersonMapper.getTotalamountByTmname();
    }

    @Override
    public List<ProtalStats> getTotalamountByYearSex() {
        return this.protalPersonMapper.getTotalamountByYearSex();
    }

    @Override
    public List<ProtalStats> getTotalamountByYearArea() {
        return this.protalPersonMapper.getTotalamountByYearArea();
    }


    @Override
    public List<ProtalStats> getTotalamountByCarryYear() {
        return this.protalPersonMapper.getTotalamountByCarryYear();
    }

    @Override
    public List<ProtalStats> getTotalamountByCarrySex() {
        return this.protalPersonMapper.getTotalamountByCarrySex();
    }

    @Override
    public List<ProtalStats> getTotalamountByCarryArea() {
        return this.protalPersonMapper.getTotalamountByCarryArea();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmnameYear() {
        return this.protalPersonMapper.getTotalamountByTmnameYear();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmnameSex() {
        return this.protalPersonMapper.getTotalamountByTmnameSex();
    }

    @Override
    public List<ProtalStats> getTotalamountByTmnameArea() {
        return this.protalPersonMapper.getTotalamountByTmnameArea();
    }

    @Override
    public List<ProtalStats> getTotalamountByYearSexArea() {
        return this.protalPersonMapper.getTotalamountByYearSexArea();
    }

    @Override
    public List<ProtalStats> getCouponByYea() {
        return this.protalPersonMapper.getCouponByYea();
    }

    @Override
    public List<ProtalStats> getCouponBySex() {
        return this.protalPersonMapper.getCouponBySex();
    }

    @Override
    public List<ProtalStats> getCouponByArea() {
        return this.protalPersonMapper.getCouponByArea();
    }

    @Override
    public List<ProtalStats> getCouponByCarry() {
        return protalPersonMapper.getCouponByCarry();
    }

    @Override
    public List<ProtalStats> getCouponByTmname() {
        return protalPersonMapper.getCouponByTmname();
    }

    @Override
    public List<ProtalStats> getCouponByYearSex() {
        return protalPersonMapper.getCouponByYearSex();
    }

    @Override
    public List<ProtalStats> getCouponByYearArea() {
        return protalPersonMapper.getCouponByYearArea();
    }

    @Override
    public List<ProtalStats> getCouponBySexArea() {
        return protalPersonMapper.getCouponBySexArea();
    }

    @Override
    public List<ProtalStats> getCouponByYearSexArea() {
        return protalPersonMapper.getCouponByYearSexArea();
    }

    @Override
    public List<ProtalStats> getCouponByByCate1() {
        return protalPersonMapper.getCouponByByCate1();
    }

    @Override
    public List<ProtalStats> getCouponByByCate2() {
        return protalPersonMapper.getCouponByByCate2();
    }

    @Override
    public List<ProtalStats> getCouponByByCate3() {
        return protalPersonMapper.getCouponByByCate3();
    }

    @Override
    public List<ProtalStats> getCouponByTmnameYea() {
        return protalPersonMapper.getCouponByTmnameYea();
    }

    @Override
    public List<ProtalStats> getCouponByTmnamSex() {
        return protalPersonMapper.getCouponByTmnamSex();
    }

    @Override
    public List<ProtalStats> getCouponByTmnameArea() {
        return protalPersonMapper.getCouponByTmnameArea();
    }

    @Override
    public List<ProtalStats> getActivtyByYea() {
        return protalPersonMapper.getActivtyByYea();
    }

    @Override
    public List<ProtalStats> getActivtyBySex() {
        return protalPersonMapper.getActivtyBySex();
    }

    @Override
    public List<ProtalStats> getActivtyByArea() {
        return protalPersonMapper.getActivtyByArea();
    }

    @Override
    public List<ProtalStats> getActivtyByCarry() {
        return protalPersonMapper.getActivtyByCarry();
    }

    @Override
    public List<ProtalStats> getActivtyByTmname() {
        return protalPersonMapper.getActivtyByTmname();
    }

    @Override
    public List<ProtalStats> getActivtyByYearSex() {
        return protalPersonMapper.getActivtyByYearSex();
    }

    @Override
    public List<ProtalStats> getActivtyByYearArea() {
        return protalPersonMapper.getActivtyByYearArea();
    }

    @Override
    public List<ProtalStats> getActivtyBySexArea() {
        return protalPersonMapper.getActivtyBySexArea();
    }

    @Override
    public List<ProtalStats> getActivtyByYearSexArea() {
        return protalPersonMapper.getActivtyByYearSexArea();
    }

    @Override
    public List<ProtalStats> getActivtyByByCate1() {
        return protalPersonMapper.getActivtyByByCate1();
    }

    @Override
    public List<ProtalStats> getActivtyByByCate2() {
        return protalPersonMapper.getActivtyByByCate2();
    }

    @Override
    public List<ProtalStats> getActivtyByByCate3() {
        return protalPersonMapper.getActivtyByByCate3();
    }

    @Override
    public List<ProtalStats> getActivtyByTmnameYea() {
        return protalPersonMapper.getActivtyByTmnameYea();
    }

    @Override
    public List<ProtalStats> getActivtyByTmnamSex() {
        return protalPersonMapper.getActivtyByTmnamSex();
    }

    @Override
    public List<ProtalStats> getActivtyByTmnameArea() {
        return protalPersonMapper.getActivtyByTmnameArea();
    }
}
