package com.bawei.persona.logger.logger;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 上海物联网学院院长：李剑
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonaLoggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonaLoggerApplication.class, args);
    }

}
