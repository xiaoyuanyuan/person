package com.bawei.persona.logger.logger.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 * * 将这个类对象的创建以及对象之间关系的维护交给Spring容器进行管理。
 *     >@Controller 控制层
 *     >@Service    业务层
 *     >@Repository 持久层
 * @RequestMapping
 *      接收什么样的请求，交给对应的方法进行处理
 * @如果使用的是Controller注解，那么类中的方法返回值是String类型，那么返回值表示跳转页面的路径
 * @ResponseBody  将返回值以字符串的形式返回给客户端
 *
 * @RestController = @Controller +   @ResponseBody
 *
 */
@RestController
public class FirstController {
    //处理客户端的请求，并且进行响应
    @RequestMapping("/first")
    public String test(){
        return "this is first";
    }
}

