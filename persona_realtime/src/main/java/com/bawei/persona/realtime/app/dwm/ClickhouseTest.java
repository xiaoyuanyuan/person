package com.bawei.persona.realtime.app.dwm;


import com.bawei.persona.realtime.bean.OrderProtalBean;
import com.bawei.persona.realtime.bean.PaymentWideNew;
import com.bawei.persona.realtime.util.ClickHouseUtil;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 上海物联网学院院长：李剑
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

public class ClickhouseTest {
    public static void main(String[] args) {
        //1.1  准备本地测试流环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        PaymentWideNew c = new PaymentWideNew();
        c.setCategory1_id(new Long(10));
        c.setCategory2_id(new Long(11));
        c.setCategory3_id(new Long(12));
        c.setCategory1_name("dsfds");
        c.setCategory2_name("sdfdsfds");
        c.setCategory3_name("sdfdsfds");
        c.setCarrier("sdfdsfds");
        c.setCarriername("sdfdsfds");
        c.setUser_gender("sdfds");
        c.setUser_gendertype("sdfdsfds");
        c.setProvince_id(new Long(10));
        c.setProvince_name("sdfdsfds");
        c.setTm_name("sdfdsfds");
        c.setActivity_reduce_amount( new BigDecimal( 12.0));
        c.setTotal_amount(new BigDecimal( 12.0));
        c.setCoupon_reduce_amount(new BigDecimal( 12.0));
        c.setSku_num(new Long(10));
        c.setOrder_price( new BigDecimal(10.0));
        c.setOrder_id(new Long(10));
        c.setUser_id(new Long(10));
        c.setPayment_create_time("2021-06-18 23:25:42");
        c.setYearbasename("dsfdsf");
        c.setYearbasetype("sdfdsf");
        c.setUser_age(20);
        c.setEmail("dsfdsfds");
        c.setEmailtype("sdfdsfdsfds");

        PaymentWideNew c1 = new PaymentWideNew();
        c1.setCategory1_id(new Long(10));
        c1.setCategory2_id(new Long(11));
        c1.setCategory3_id(new Long(12));
        c1.setCategory1_name("dsfds");
        c1.setCategory2_name("sdfdsfds");
        c1.setCategory3_name("sdfdsfds");
        c1.setCarrier("sdfdsfds");
        c1.setCarriername("sdfdsfds");
        c1.setUser_gender("sdfds");
        c1.setUser_gendertype("sdfdsfds");
        c1.setProvince_id(new Long(10));
        c1.setProvince_name("sdfdsfds");
        c1.setTm_name("sdfdsfds");
        c1.setActivity_reduce_amount( new BigDecimal( 12.0));
        c1.setTotal_amount(new BigDecimal( 12.0));
        c1.setCoupon_reduce_amount(new BigDecimal( 12.0));
        c1.setSku_num(new Long(10));
        c1.setOrder_price( new BigDecimal(10.0));
        c1.setOrder_id(new Long(10));
        c1.setUser_id(new Long(10));
        c1.setPayment_create_time("2021-06-18 23:25:42");
        c1.setYearbasename("dsfdsf");
        c1.setYearbasetype("sdfdsf");
        c1.setUser_age(20);
        c1.setEmail("dsfdsfds");
        c1.setEmailtype("sdfdsfdsfds");
        //1.2 设置并行度
        env.setParallelism(4);
        DataStreamSource<PaymentWideNew> orderProtalBeanDataStreamSource = env.fromCollection(Arrays.asList(c, c1));

        orderProtalBeanDataStreamSource.addSink(
                ClickHouseUtil.getJdbcSink(" insert into  insert into protrait_order(category1_id,category2_id,category3_id,category1_name,category2_name, category3_name,carrier,carriername, user_gendertype,user_gender,province_id,province_name,tm_name,email, emailtype,user_age,yearbasetype,yearbasename,create_time,user_id,order_id,order_price,sku_num ,coupon_reduce_amount,total_amount,activity_reduce_amount) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        );
    }

    public static void main1(String[] args) {

        //1.1  准备本地测试流环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        OrderProtalBean c = new OrderProtalBean();
        c.setCategory1_id(new Long(10));
        c.setCategory2_id(new Long(11));
        c.setCategory3_id(new Long(12));
        c.setCategory1_name("dsfds");
        c.setCategory2_name("sdfdsfds");
        c.setCategory3_name("sdfdsfds");
        c.setCarrier("sdfdsfds");
        c.setCarriername("sdfdsfds");
        c.setUser_gender("sdfds");
        c.setUser_gendertype("sdfdsfds");
        c.setProvince_id("dsfdsfds");
        c.setProvince_name("sdfdsfds");
        c.setTm_name("sdfdsfds");
        c.setActivity_reduce_amount( new BigDecimal( 12.0));
        c.setTotal_amount(new BigDecimal( 12.0));
        c.setCoupon_reduce_amount(new BigDecimal( 12.0));
        c.setSku_num(new Long(10));
        c.setOrder_price( new BigDecimal(10.0));
        c.setOrder_id(new Long(10));
        c.setUser_id(new Long(10));
        c.setCreate_time("2021-06-18 23:25:42");
        c.setYearbasename("dsfdsf");
        c.setYearbasetype("sdfdsf");
        c.setUser_age("20");
        c.setEmail("dsfdsfds");
        c.setEmailtype("sdfdsfdsfds");

        OrderProtalBean c1 = new OrderProtalBean();
        c.setCategory1_id(new Long(13));
        c.setCategory2_id(new Long(14));
        c.setCategory3_id(new Long(15));
        c1.setCategory1_name("dsfds");
        c1.setCategory2_name("sdfdsfds");
        c1.setCategory3_name("sdfdsfds");
        c1.setCarrier("sdfdsfds");
        c1.setCarriername("sdfdsfds");
        c1.setUser_gender("sdfds");
        c1.setUser_gendertype("sdfdsfds");
        c1.setProvince_id("dsfdsfds");
        c1.setProvince_name("sdfdsfds");
        c1.setTm_name("sdfdsfds");
        c1.setActivity_reduce_amount( new BigDecimal( 12.0));
        c1.setTotal_amount(new BigDecimal( 12.0));
        c1.setCoupon_reduce_amount(new BigDecimal( 12.0));
        c1.setSku_num(new Long(10));
        c1.setOrder_price( new BigDecimal(10.0));
        c1.setOrder_id(new Long(10));
        c1.setUser_id(new Long(10));
        c1.setCreate_time("2021-06-18 23:25:42");
        c1.setYearbasename("dsfdsf");
        c1.setYearbasetype("sdfdsf");
        c1.setUser_age("20");
        c1.setEmail("dsfdsfds");
        c1.setEmailtype("sdfdsfdsfds");
        //1.2 设置并行度
        env.setParallelism(4);
        DataStreamSource<OrderProtalBean> orderProtalBeanDataStreamSource = env.fromCollection(Arrays.asList(c, c1));

        orderProtalBeanDataStreamSource.addSink(
                ClickHouseUtil.getJdbcSink("insert into  insert into protrait_order(category1_id,category2_id,category3_id,category1_name,category2_name, category3_name,carrier,carriername, user_gendertype,user_gender,province_id,province_name,tm_name,email, emailtype,user_age,yearbasetype,yearbasename,create_time,user_id,order_id,order_price,sku_num ,coupon_reduce_amount,total_amount,activity_reduce_amount) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        );

    }

}
