package com.bawei.persona.realtime.app.dws;

import java.io.Serializable;

/**
 * 上海大数据学院
 * 项目规划及管理：李剑
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
public class BeanV  implements Serializable{

    String  ar  ;
    String  mid  ;
    String  md  ;

    public BeanV() {
    }



    String  is_new  ;
    String  os  ;
    String  ch  ;


    String  uid  ;

    String  vc  ;
    String  ba  ;



    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public String getIs_new() {
        return is_new;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public BeanV(String ar, String mid, String md, String is_new, String os, String ch, String uid, String vc, String ba, Long ts) {
        this.ar = ar;
        this.mid = mid;
        this.md = md;
        this.is_new = is_new;
        this.os = os;
        this.ch = ch;
        this.uid = uid;
        this.vc = vc;
        this.ba = ba;
        this.ts = ts;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVc() {
        return vc;
    }

    public void setVc(String vc) {
        this.vc = vc;
    }

    public String getBa() {
        return ba;
    }

    public void setBa(String ba) {
        this.ba = ba;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    @Override
    public String toString() {
        return "BeanV{" +
                "ar='" + ar + '\'' +
                ", mid='" + mid + '\'' +
                ", md='" + md + '\'' +
                ", is_new='" + is_new + '\'' +
                ", os='" + os + '\'' +
                ", ch='" + ch + '\'' +
                ", uid='" + uid + '\'' +
                ", vc='" + vc + '\'' +
                ", ba='" + ba + '\'' +
                ", ts=" + ts +
                '}';
    }

    Long   ts ;

}
