package com.bawei.persona.realtime.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 上海大数据学院
 * 项目规划及管理：李剑
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponInfo implements Serializable {
    String id;
    String coupon_name;
    String coupon_type;
    String condition_amount;
    String condition_num;
    String activity_id;
    String benefit_amount;
    String benefit_discount;
    String create_time;
    String range_type;
    String limit_num;
    String taken_count;
    String start_time;
    String end_time;
    String operate_time;
    String expire_time;
    String range_desc;

}
