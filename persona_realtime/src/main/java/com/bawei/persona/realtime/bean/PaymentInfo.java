package com.bawei.persona.realtime.bean;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
@Data
public class PaymentInfo {
    Long create_ts;
    Long id;
    Long order_id;
    Long user_id;
    BigDecimal total_amount;
    String subject;
    String payment_type;
    String create_time;
    String callback_time;
    Long order_create_ts;
    String out_trade_no ;
    String trade_no ;

}

