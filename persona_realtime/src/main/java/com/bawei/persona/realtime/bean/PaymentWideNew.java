package com.bawei.persona.realtime.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentWideNew implements Serializable {


    Long payment_id;
    String subject;
    String payment_type;
    String payment_create_time;
    String callback_time;
    Long detail_id;
    Long order_id ;
    Long sku_id;
    BigDecimal order_price ;
    Long sku_num ;
    String sku_name;
    Long province_id;
    String order_status;
    Long user_id;
    BigDecimal total_amount;
    BigDecimal payment_total_amount;

    BigDecimal activity_reduce_amount;
    BigDecimal coupon_reduce_amount;
    BigDecimal original_total_amount;
    BigDecimal feight_fee;
   // BigDecimal split_feight_fee;
    BigDecimal split_activity_amount;
    BigDecimal split_coupon_amount;
    BigDecimal split_total_amount;
    String order_create_time;

    String province_name;//查询维表得到
    String province_area_code;
    String province_iso_code;
    String province_3166_2_code;
    Integer user_age ;
    String user_gender;

    Long spu_id;     //作为维度数据 要关联进来
    Long tm_id;
    Long category3_id;
    String spu_name;
    String tm_name;
    String category3_name;
    String out_trade_no ;
//将来可能保存的Ts 需要的数据
    Long order_create_ts;
    Long payment_create_ts;
    String trade_no ;

    // 新添加的用户画像所用的维度
    Long category1_id ;
    Long category2_id ;
    String category1_name ;
    String category2_name ;
    String carrier ;
    String carriername ;
    String user_gendertype ;
    String emailtype ;
    String yearbasetype ;
    String yearbasename ;
    String email ;









    public PaymentWideNew(PaymentInfo paymentInfo, OrderWidePersona orderWide){
        mergeOrderWide(orderWide);
        mergePaymentInfo(paymentInfo);
    }

    public void  mergePaymentInfo(PaymentInfo paymentInfo)  {
        if (paymentInfo != null) {
            try {
                BeanUtils.copyProperties(this,paymentInfo);
                payment_create_time=paymentInfo.create_time;
                payment_id = paymentInfo.id;
                this.payment_create_ts = paymentInfo.create_ts ;
                this.out_trade_no = paymentInfo.out_trade_no ;
                this.payment_total_amount = paymentInfo.total_amount;
                this.trade_no = paymentInfo.trade_no;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public void  mergeOrderWide(OrderWidePersona orderWide)  {
        if (orderWide != null) {
            try {
                BeanUtils.copyProperties(this,orderWide);
                order_create_time=orderWide.create_time;
                this.order_create_ts = orderWide.create_ts ;
                this.spu_id = orderWide.spu_id ;
                this.feight_fee = orderWide.feight_fee ;
                this.category1_id = orderWide.category1_id ;

                this.category2_id = orderWide.category2_id ;
                this.category1_name = orderWide.category1_name ;
                this.category2_name = orderWide.category2_name ;
                this.carrier = orderWide.carrier ;
                this.carriername = orderWide.carriername ;
                this.user_gendertype = orderWide.user_gendertype ;
                this.emailtype = orderWide.emailtype ;
                this.yearbasetype = orderWide.yearbasetype ;
                this.yearbasename = orderWide.yearbasename ;
                this.email = orderWide.email;


            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}
