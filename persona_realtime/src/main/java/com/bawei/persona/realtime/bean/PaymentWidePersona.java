package com.bawei.persona.realtime.bean;

import lombok.Builder;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
public class PaymentWidePersona {

//  clickhouse 需要统计的按支付统计表数据
    @Builder.Default
    Long category1_id =0L;
    @Builder.Default
    Long category2_id =0L ;
    @Builder.Default
    Long category3_id =0L;
    String category1_name ;
    String category2_name ;
    String category3_name;

    String carrier ;
    String carriername ;
    String user_gendertype ;
    String user_gender;
    @Builder.Default
    Long province_id=0L;
    String province_name;//查询维表得到
    String tm_name;
    String email ;
    String emailtype ;
    @Builder.Default
    Integer user_age = 0 ;

    String yearbasetype ;
    String yearbasename ;
    String payment_create_time;
    @Builder.Default
    Long user_id=0L;
    @Builder.Default
    Long order_id =0L;
    @Builder.Default
    BigDecimal order_price = BigDecimal.ZERO ;
    @Builder.Default
    Long sku_num =0L;
    @Builder.Default
    BigDecimal coupon_reduce_amount = BigDecimal.ZERO;
    @Builder.Default
    BigDecimal total_amount = BigDecimal.ZERO;
    @Builder.Default
    BigDecimal activity_reduce_amount = BigDecimal.ZERO;
    @Builder.Default
    BigDecimal payment_total_amount = BigDecimal.ZERO;
    String spu_name;

    //String sku_name ;

    ///////////不需要插入clickhouse 的数据///////////////
     @Builder.Default
     @TransientSink
     Long payment_id=0L;
    @TransientSink
    String subject;
    @TransientSink
    String payment_type;

    @TransientSink
    String callback_time;
    @Builder.Default
    @TransientSink
    Long detail_id=0L;
    @Builder.Default
    @TransientSink
    Long sku_id=0L;

    @TransientSink
    String order_status;
    @TransientSink
    BigDecimal original_total_amount;
    @Builder.Default
    @TransientSink
    BigDecimal feight_fee=BigDecimal.ZERO;
    // BigDecimal split_feight_fee;
    @Builder.Default
    @TransientSink
    BigDecimal split_activity_amount=BigDecimal.ZERO;
    @Builder.Default
    @TransientSink
    BigDecimal split_coupon_amount=BigDecimal.ZERO;
    @Builder.Default
    @TransientSink
    BigDecimal split_total_amount=BigDecimal.ZERO;
    @TransientSink
    String order_create_time;
    @TransientSink
    String province_area_code;
    @TransientSink
    String province_iso_code;
    @TransientSink
    String province_3166_2_code;
    @Builder.Default
    @TransientSink
    Long spu_id =0L;     //作为维度数据 要关联进来
    @Builder.Default
    @TransientSink
    Long tm_id=0L;
    @TransientSink
     String sku_name ;
    @TransientSink
    String out_trade_no ;
    //将来可能保存的Ts 需要的数据
    @Builder.Default
    @TransientSink
    Long order_create_ts =0L;
    @Builder.Default
    @TransientSink
    Long payment_create_ts=0L;
    @TransientSink
    String trade_no ;



    public PaymentWidePersona(PaymentInfo paymentInfo, OrderWidePersona orderWide){
        mergeOrderWide(orderWide);
        mergePaymentInfo(paymentInfo);
    }

    public void  mergePaymentInfo(PaymentInfo paymentInfo)  {
        if (paymentInfo != null) {
            try {
                BeanUtils.copyProperties(this,paymentInfo);
                payment_create_time=paymentInfo.create_time;
                payment_id = paymentInfo.id;
                this.payment_create_ts = paymentInfo.create_ts ;
                this.out_trade_no = paymentInfo.out_trade_no ;
                this.payment_total_amount = paymentInfo.total_amount;
                this.trade_no = paymentInfo.trade_no;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public void  mergeOrderWide(OrderWidePersona orderWide)  {
        if (orderWide != null) {
            try {
                BeanUtils.copyProperties(this,orderWide);
                order_create_time=orderWide.create_time;
                this.order_create_ts = orderWide.create_ts ;
                this.spu_id = orderWide.spu_id ;
                this.feight_fee = orderWide.feight_fee ;
                this.category1_id = orderWide.category1_id ;

                this.category2_id = orderWide.category2_id ;
                this.category1_name = orderWide.category1_name ;
                this.category2_name = orderWide.category2_name ;
                this.carrier = orderWide.carrier ;
                this.carriername = orderWide.carriername ;
                this.user_gendertype = orderWide.user_gendertype ;
                this.emailtype = orderWide.emailtype ;
                this.yearbasetype = orderWide.yearbasetype ;
                this.yearbasename = orderWide.yearbasename ;
                this.email = orderWide.email;
                this.category3_id = orderWide.category3_id ;
                this.category3_name = orderWide.category3_name ;
                this.user_gender = orderWide.user_gender ;
                this.province_id = orderWide.province_id ;
                this.province_name = orderWide.province_name ;
                this.tm_name = orderWide.tm_name ;
                this.order_price = orderWide.order_price ;
                this.sku_num = orderWide.sku_num ;
                this.coupon_reduce_amount = orderWide.coupon_reduce_amount ;
                this.total_amount = orderWide.total_amount ;
                this.activity_reduce_amount = orderWide.activity_reduce_amount ;
                this.user_age = orderWide.user_age ;
                this.user_id = orderWide.user_id ;
                this.order_id = orderWide.order_id ;
                this.spu_name = orderWide.spu_name ;
                this.sku_name = orderWide.sku_name ;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }


}
