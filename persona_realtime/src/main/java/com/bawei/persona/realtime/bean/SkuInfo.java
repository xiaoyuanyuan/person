package com.bawei.persona.realtime.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkuInfo implements Serializable {
    String id;
    String spu_id;
    String price;
    String sku_name;
    String sku_desc;
    String weight;
    String tm_id;
    String category3_id;
    String sku_default_img;
    String is_sale;
    String create_time;

}
