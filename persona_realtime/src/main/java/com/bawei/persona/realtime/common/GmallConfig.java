package com.bawei.persona.realtime.common;


/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */

public class GmallConfig {
    //Hbase的命名空间
    public static final String HABSE_SCHEMA = "GMALL0820_REALTIME";

//    public static final String HBASE_ZOOKEEPER_QUORUM = "hadoop102,hadoop103,hadoop104";
    public static final String HBASE_ZOOKEEPER_QUORUM = "hadoop102";

    public static final String HBASE_ZOOKEEPER_PROPERTY_CLIENTPORT = "2181";

    public static final String DIM_PROVICE = "dim_base_province";
    public static final String DIM_USER_INFO = "dim_user_info";


    public static final String DIM_SKU_INFO = "dim_sku_info";
    public static final String DIM_SPU_INFO = "dim_spu_info";
    public static final String DIM_BASE_CATEGORY3 = "dim_base_category3";
    public static final String DIM_BASE_TRADEMARK = "dim_base_trademark";


















    //Phonenix连接的服务器地址
  //  public static final String PHOENIX_SERVER="jdbc:phoenix:hadoop102,hadoop103,hadoop104:2181";
    public static final String PHOENIX_SERVER="jdbc:phoenix:hadoop102:2181";


    //ClickHouse的URL连接地址
    public static final String CLICKHOUSE_URL="jdbc:clickhouse://hadoop102:8123/default";

}
