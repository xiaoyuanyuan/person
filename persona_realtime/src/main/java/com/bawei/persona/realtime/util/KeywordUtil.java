package com.bawei.persona.realtime.util;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目规划及管理
 * 上海大数据学院院长 ：孙丰朝
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since @Date: 2021/9/3 8:47

 * Desc: IK分词器分词工具类
 */
public class KeywordUtil {
    //分词    将字符串进行分词，将分词之后的结果放到一个集合中返回
    public static List<String> analyze(String text){
        List<String> wordList = new ArrayList<>();
        //将字符串转换为字符输入流
        StringReader sr = new StringReader(text);
        //创建分词器对象
        IKSegmenter ikSegmenter = new IKSegmenter(sr, true);
        // Lexeme  是分词后的一个单词对象
        Lexeme lexeme = null;
        //通过循环，获取分词后的数据
        while(true){
            try {
                //获取一个单词
                if((lexeme = ikSegmenter.next())!=null){
                    String word = lexeme.getLexemeText();
                    wordList.add(word);
                }else{
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return wordList;
    }

    public static void main(String[] args) {
        String text = "八维大数据数仓";
        System.out.println(KeywordUtil.analyze(text));

    }
}
