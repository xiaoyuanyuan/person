package com.bawei.persona.realtime.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bawei.persona.realtime.bean.BaseCategory1;


/**
 * 上海大数据学院
 * 项目规划及管理：李剑
 * 技术指导及需求分析：郭洵
 * 编程：楚志高
 *
 * @author bawei  bigdata sh
 * @since 2021-06-11
 */
public class Test {
    public static void main(String[] args) {
       String cc ="{\"database\":\"gmall2021\",\"table\":\"base_category1\",\"type\":\"insert\",\"ts\":1624295990,\"xid\":603819,\"xoffset\":369120,\"data\":{\"id\":1407025480462028812,\"name\":\"zhigao\"}}\n" ;

         JSONObject jsonObject = JSON.parseObject(cc);
        String data = jsonObject.getString("data");
        System.out.println(data);

        JSONObject jsonObject1 = JSON.parseObject(data);

        BaseCategory1 baseCategory1 = JSON.toJavaObject(jsonObject1, BaseCategory1.class);
        System.out.println( baseCategory1.getId() + " " + baseCategory1.getName());


    }
}
